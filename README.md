## An example webpage

This repository shows an example *minimal* configuration for a static
website with GitLab Pages and Jekyll.

--

Check out the following sources:

* [`Gemfile`](Gemfile): Jekyll and dependencies
* [`_config.yml`](_config.yml): Page configuration
* [`.gitlab-ci.yml`](.gitlab-ci.yml): GitLab CI/CD configuration that builds the web


